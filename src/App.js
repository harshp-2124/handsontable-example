import React from "react";
import {HotTable} from "@handsontable/react";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.handsontableData = [
      {},
      {type: "numeric"},
      {
        type: "dropdown",
        source: [
          "yellow",
          "red",
          "orange",
          "green",
          "blue",
          "gray",
          "black",
          "white",
        ],
        placeholder: "yellow"
      },
      {
        type: "dropdown",
        source: [
          "yellow",
          "red",
          "orange",
          "green",
          "blue",
          "gray",
          "black",
          "white",
        ],
        placeholder: "yellow"
      },
    ];
  }

  render() {
    return (
      <div>
        <HotTable
          id="hot"
          columns={this.handsontableData}
          colHeaders={true}
          rowHeaders={true}
          licenseKey="non-commercial-and-evaluation"
        />
      </div>
    );
  }
}

export default App;
